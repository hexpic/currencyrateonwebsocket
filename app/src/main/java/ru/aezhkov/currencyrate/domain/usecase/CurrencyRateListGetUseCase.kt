package ru.aezhkov.currencyrate.domain.usecase

import io.reactivex.Flowable
import ru.aezhkov.currencyrate.data.model.CurrencyPair
import ru.aezhkov.currencyrate.domain.model.CurrencyModel

interface CurrencyRateListGetUseCase {

    fun observeCurrencyList(): Flowable<List<CurrencyModel>>

    fun removePair(pair: CurrencyPair)
}