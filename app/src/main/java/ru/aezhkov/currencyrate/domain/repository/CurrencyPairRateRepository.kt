package ru.aezhkov.currencyrate.domain.repository

import com.tinder.scarlet.State
import io.reactivex.Flowable
import ru.aezhkov.currencyrate.data.model.CurrencyPair
import ru.aezhkov.currencyrate.domain.model.CurrencyModel

interface CurrencyPairRateRepository {

    fun subscribe(pair: CurrencyPair)

    fun unsubscribe(pair: CurrencyPair)

    fun observe(): Flowable<CurrencyModel>

    fun observeState(): Flowable<State>
}