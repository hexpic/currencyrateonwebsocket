package ru.aezhkov.currencyrate.domain.di

import dagger.Binds
import dagger.Module
import ru.aezhkov.currencyrate.domain.interactor.CurrencyRateListGetInteractor
import ru.aezhkov.currencyrate.domain.interactor.CurrencyPairVisibilityInteractor
import ru.aezhkov.currencyrate.domain.interactor.RatesListGetInteractor
import ru.aezhkov.currencyrate.domain.usecase.CurrencyRateListGetUseCase
import ru.aezhkov.currencyrate.domain.usecase.CurrencyPairVisibilityUseCase
import ru.aezhkov.currencyrate.domain.usecase.RatesListGetUseCase
import javax.inject.Singleton


@Module
interface SingletonDomainBinder {

    @Singleton
    @Binds
    fun provideCurrencyPairVisibilityUseCase(impl: CurrencyPairVisibilityInteractor): CurrencyPairVisibilityUseCase
}