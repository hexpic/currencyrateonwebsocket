package ru.aezhkov.currencyrate.domain.model

import ru.aezhkov.currencyrate.data.model.CurrencyPair

data class CurrencyModel(
    val pair: CurrencyPair,
    val instrument: String,
    val bid: String,
    val ask: String,
    val spread: String
)
