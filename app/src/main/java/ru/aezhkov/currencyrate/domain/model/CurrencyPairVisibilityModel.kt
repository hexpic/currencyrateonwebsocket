package ru.aezhkov.currencyrate.domain.model

import ru.aezhkov.currencyrate.data.model.CurrencyPair

data class CurrencyPairVisibilityModel(
    val pair: CurrencyPair,
    val isVisibile: Boolean
)