package ru.aezhkov.currencyrate.domain.interactor

import com.tinder.scarlet.State
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import ru.aezhkov.currencyrate.data.CurrencyPairStorage
import ru.aezhkov.currencyrate.data.model.CurrencyPair
import ru.aezhkov.currencyrate.domain.model.CurrencyModel
import ru.aezhkov.currencyrate.domain.model.CurrencyPairVisibilityModel
import ru.aezhkov.currencyrate.domain.repository.CurrencyPairRateRepository
import ru.aezhkov.currencyrate.domain.usecase.CurrencyPairVisibilityUseCase
import ru.aezhkov.currencyrate.domain.usecase.CurrencyRateListGetUseCase
import javax.inject.Inject

class CurrencyRateListGetInteractor
@Inject constructor(
    private val currencyPairVisibilityUseCase: CurrencyPairVisibilityUseCase,
    private val storage: CurrencyPairStorage,
    private val repository: CurrencyPairRateRepository
) : CurrencyRateListGetUseCase {

    private var subscribe = CompositeDisposable()
    private val receivedPairMap = HashMap<CurrencyPair, CurrencyModel>()
    private val itemsProcessor = PublishProcessor.create<List<CurrencyModel>>()

    override fun observeCurrencyList(): Flowable<List<CurrencyModel>> {
        return itemsProcessor
            .doOnSubscribe {
                observeVisibilityChanges()
                observeRateValueChanges()
                initSubscriptions()
            }
            .doOnTerminate {
                subscribe.clear()
            }
            .subscribeOn(Schedulers.io())
    }

    private fun observeRateValueChanges() {
        subscribe.add(
            repository.observe()
                .map { model ->
                    synchronized(this) {
                        if (storage.getValue(model.pair)) {
                            receivedPairMap[model.pair] = model
                        }
                    }
                    receivedPairMap.values
                        .sortedBy { it.pair }
                        .toList()
                }
                .onErrorResumeNext { _: Throwable ->
                    Flowable.just(receivedPairMap.values.toList())
                }
                .subscribe({ itemsProcessor.onNext(it) }, { itemsProcessor.onError(it) }))
    }

    private fun initSubscriptions() {
        subscribe.add(
            repository.observeState()
                .subscribe { event ->
                    if (event is State.Connected) {
                        CurrencyPair.values().map {
                            val visibility = storage.getValue(it)
                            if (visibility) {
                                repository.subscribe(it)
                            }
                        }
                    }
                }
        )
    }

    override fun removePair(pair: CurrencyPair) {
        synchronized(this) {
            currencyPairVisibilityUseCase.setCurrencyPairVisibility(CurrencyPairVisibilityModel(pair, false))
        }
        removePairAndReEmitList(pair)
    }

    private fun removePairAndReEmitList(pair: CurrencyPair) {
        synchronized(this) {
            receivedPairMap.remove(pair)
        }
        val values = receivedPairMap.values
            .sortedBy { it.pair }
            .toList()
        itemsProcessor.onNext(values)
    }

    private fun observeVisibilityChanges() {
        subscribe.add(
            currencyPairVisibilityUseCase.observeCurrencyPairsVisibility()
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (it.isVisibile) {
                        repository.subscribe(it.pair)
                    } else {
                        repository.unsubscribe(it.pair)
                        removePairAndReEmitList(it.pair)
                    }
                }, {
                    it.toString()//todo make something if get error
                })
        )
    }
}