package ru.aezhkov.currencyrate.domain.usecase

import io.reactivex.Observable
import ru.aezhkov.currencyrate.domain.model.CurrencyPairVisibilityModel

interface CurrencyPairVisibilityUseCase {

    fun setCurrencyPairVisibility(model: CurrencyPairVisibilityModel)

    fun observeCurrencyPairsVisibility(): Observable<CurrencyPairVisibilityModel>

}