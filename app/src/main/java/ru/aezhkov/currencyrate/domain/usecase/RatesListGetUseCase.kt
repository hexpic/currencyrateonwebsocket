package ru.aezhkov.currencyrate.domain.usecase

import io.reactivex.Observable
import ru.aezhkov.currencyrate.domain.model.CurrencyPairVisibilityModel

interface RatesListGetUseCase {

    fun observeRatesList(): Observable<List<CurrencyPairVisibilityModel>>
}