package ru.aezhkov.currencyrate.domain.interactor

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import ru.aezhkov.currencyrate.data.CurrencyPairStorage
import ru.aezhkov.currencyrate.data.model.CurrencyPair
import ru.aezhkov.currencyrate.domain.model.CurrencyPairVisibilityModel
import ru.aezhkov.currencyrate.domain.usecase.CurrencyPairVisibilityUseCase
import javax.inject.Inject

class CurrencyPairVisibilityInteractor
@Inject constructor(
    private val storage: CurrencyPairStorage
) : CurrencyPairVisibilityUseCase {

    private val visibilitySubject = PublishSubject.create<CurrencyPairVisibilityModel>()

    override fun setCurrencyPairVisibility(model: CurrencyPairVisibilityModel) {
        storage.setValue(model.pair, model.isVisibile)
        visibilitySubject.onNext(model)
    }

    override fun observeCurrencyPairsVisibility(): Observable<CurrencyPairVisibilityModel> {
        return visibilitySubject
    }
}