package ru.aezhkov.currencyrate.domain.interactor

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import ru.aezhkov.currencyrate.data.CurrencyPairStorage
import ru.aezhkov.currencyrate.data.model.CurrencyPair
import ru.aezhkov.currencyrate.domain.model.CurrencyPairVisibilityModel
import ru.aezhkov.currencyrate.domain.usecase.CurrencyPairVisibilityUseCase
import ru.aezhkov.currencyrate.domain.usecase.RatesListGetUseCase
import javax.inject.Inject

class RatesListGetInteractor
@Inject constructor(
    private val storage: CurrencyPairStorage,
    private val currencyPairVisibilityUseCase: CurrencyPairVisibilityUseCase
) : RatesListGetUseCase {

    private val subject = BehaviorSubject.create<List<CurrencyPairVisibilityModel>>()

    private var disposable: Disposable? = null

    override fun observeRatesList(): Observable<List<CurrencyPairVisibilityModel>> {
        return subject
            .doOnSubscribe {
                initRateList()
                observeRatePairVisibility()
            }
            .doOnDispose {
                disposable?.dispose()
            }
    }

    private fun observeRatePairVisibility() {
        disposable = currencyPairVisibilityUseCase.observeCurrencyPairsVisibility()
            .subscribe {
                val ratePairList = getRatePairList()
                subject.onNext(ratePairList)
            }
    }

    private fun initRateList() {
        val initRatesList = getRatePairList()
        subject.onNext(initRatesList)
    }

    private fun getRatePairList(): List<CurrencyPairVisibilityModel> {
        return CurrencyPair.values().map {
            val pairVisibility = storage.getValue(it)
            CurrencyPairVisibilityModel(it, pairVisibility)
        }
    }
}