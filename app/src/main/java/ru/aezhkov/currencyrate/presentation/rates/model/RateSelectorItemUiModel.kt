package ru.aezhkov.currencyrate.presentation.rates.model

import ru.aezhkov.currencyrate.data.model.CurrencyPair

data class RateSelectorItemUiModel(
    val pair: CurrencyPair,
    val title: String,
    val checked: Boolean
) {
    var checkedListener: ((CurrencyPair, isChecked: Boolean) -> Unit)? = null
}
