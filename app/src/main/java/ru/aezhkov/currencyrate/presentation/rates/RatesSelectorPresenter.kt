package ru.aezhkov.currencyrate.presentation.rates

import io.reactivex.android.schedulers.AndroidSchedulers
import ru.aezhkov.currencyrate.data.model.CurrencyPair
import ru.aezhkov.currencyrate.domain.model.CurrencyPairVisibilityModel
import ru.aezhkov.currencyrate.domain.usecase.CurrencyPairVisibilityUseCase
import ru.aezhkov.currencyrate.domain.usecase.RatesListGetUseCase
import ru.aezhkov.currencyrate.presentation.base.presenter.BasePresenter
import ru.aezhkov.currencyrate.presentation.rates.mapper.RateSelectionUiMapper
import ru.aezhkov.currencyrate.presentation.rates.model.RateSelectorItemUiModel
import javax.inject.Inject

class RatesSelectorPresenter
@Inject constructor(
    private val rateListGetUseCase: RatesListGetUseCase,
    private val uiMapper: RateSelectionUiMapper,
    private val currencyPairVisibilityUseCase: CurrencyPairVisibilityUseCase
) : BasePresenter<RateSelectorView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        unsubscribeOnDestroy(
            rateListGetUseCase.observeRatesList()
                .map { mapRatesList(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.updateList(it)
                }, {
                    it.toString()
                    // handle error
                })
        )
    }

    private fun mapRatesList(list: List<CurrencyPairVisibilityModel>): List<RateSelectorItemUiModel> {
        return list.map {
            uiMapper.map(it).apply {
                checkedListener = { pair, checked -> changeRatePairVisibility(pair, checked) }
            }
        }
    }

    private fun changeRatePairVisibility(pair: CurrencyPair, checked: Boolean) {
        val model = CurrencyPairVisibilityModel(pair, checked)
        currencyPairVisibilityUseCase.setCurrencyPairVisibility(model)
    }
}