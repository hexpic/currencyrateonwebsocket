package ru.aezhkov.currencyrate.presentation.base.widget

interface Widget<MODEL> {

    fun bind(model: MODEL)
}