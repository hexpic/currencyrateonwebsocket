package ru.aezhkov.currencyrate.presentation.list

import io.reactivex.android.schedulers.AndroidSchedulers
import ru.aezhkov.currencyrate.data.model.CurrencyPair
import ru.aezhkov.currencyrate.domain.model.CurrencyModel
import ru.aezhkov.currencyrate.domain.usecase.CurrencyRateListGetUseCase
import ru.aezhkov.currencyrate.presentation.base.presenter.BasePresenter
import ru.aezhkov.currencyrate.presentation.list.mapper.CurrencyRateUiMapper
import ru.aezhkov.currencyrate.presentation.list.model.CurrencyRateUiModel
import javax.inject.Inject

class CurrencyRateListPresenter
@Inject constructor(
    private val currencyListGetUseCase: CurrencyRateListGetUseCase,
    private val currencyRateUiMapper: CurrencyRateUiMapper
) : BasePresenter<CurrencyRateListView>() {
    private var spreadSort = SortSpread.DEFAULT

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        unsubscribeOnDestroy(
            currencyListGetUseCase.observeCurrencyList()
                .map { models -> // еще можно перенести этот кусок в интерактор и эмитить хранящийся там список значений, на случай если тики будут реже.
                    sortModel(models)
                }
                .map { mapItems(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.updateList(it)
                }, {
                    it.toString()
                })
        )
    }

    private fun mapItems(models: List<CurrencyModel>): List<CurrencyRateUiModel> {
        return models.map {
            currencyRateUiMapper.map(it).apply {
                longClickListener = { pair ->
                    removeItem(pair)
                }
            }
        }
    }

    private fun removeItem(currencyPair: CurrencyPair) {
        currencyListGetUseCase.removePair(currencyPair)
    }

    private fun sortModel(models: List<CurrencyModel>): List<CurrencyModel> {
        return if (spreadSort == SortSpread.ASC) {
            models.sortedBy { it.spread.toFloat() }.reversed()
        } else {
            models
        }
    }

    fun sortSpread() {
        spreadSort = if (spreadSort == SortSpread.DEFAULT) {
            SortSpread.ASC
        } else {
            SortSpread.DEFAULT
        }
    }

    private enum class SortSpread {
        DEFAULT,
        ASC
    }
}