package ru.aezhkov.currencyrate.presentation.list

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType
import ru.aezhkov.currencyrate.presentation.list.model.CurrencyRateUiModel

@StateStrategyType(AddToEndSingleStrategy::class)
interface CurrencyRateListView : MvpView {

    fun updateList(models: List<CurrencyRateUiModel>)
}