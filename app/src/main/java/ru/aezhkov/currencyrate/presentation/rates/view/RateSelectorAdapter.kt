package ru.aezhkov.currencyrate.presentation.rates.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.aezhkov.currencyrate.presentation.base.widget.Widget
import ru.aezhkov.currencyrate.presentation.list.model.CurrencyRateUiModel
import ru.aezhkov.currencyrate.R
import ru.aezhkov.currencyrate.presentation.rates.model.RateSelectorItemUiModel

class RateSelectorAdapter : ListAdapter<RateSelectorItemUiModel, RateSelectorItemViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateSelectorItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val rateItemView = inflater.inflate(R.layout.rate_selector_item_view, parent, false)
        return RateSelectorItemViewHolder(rateItemView)
    }

    override fun onBindViewHolder(holder: RateSelectorItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<RateSelectorItemUiModel>() {

            override fun areItemsTheSame(oldItem: RateSelectorItemUiModel, newItem: RateSelectorItemUiModel): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: RateSelectorItemUiModel, newItem: RateSelectorItemUiModel): Boolean {
                return oldItem == newItem
            }
        }
    }
}

class RateSelectorItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(model: RateSelectorItemUiModel) {
        itemView as Widget<RateSelectorItemUiModel>
        itemView.bind(model)
    }

}