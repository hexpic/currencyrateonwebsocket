package ru.aezhkov.currencyrate.presentation.base

import android.app.Application
import ru.aezhkov.currencyrate.presentation.base.di.ApplicationComponent
import ru.aezhkov.currencyrate.presentation.base.di.DaggerApplicationComponent
import ru.aezhkov.currencyrate.presentation.list.di.CurrencyRateListComponent
import ru.aezhkov.currencyrate.presentation.list.di.DaggerCurrencyRateListComponent
import ru.aezhkov.currencyrate.presentation.rates.di.DaggerRatesSelectorComponent
import ru.aezhkov.currencyrate.presentation.rates.di.RatesSelectorComponent

class RateApp : Application() {

    lateinit var applicationComponent: ApplicationComponent
    lateinit var currencyRateListComponent: CurrencyRateListComponent
    lateinit var rateSelectorListComponent: RatesSelectorComponent

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.builder()
            .application(this)
            .build()

        currencyRateListComponent = DaggerCurrencyRateListComponent.builder()
            .dependency(applicationComponent)
            .build()

        rateSelectorListComponent = DaggerRatesSelectorComponent.builder()
            .dependency(applicationComponent)
            .build()
    }
}