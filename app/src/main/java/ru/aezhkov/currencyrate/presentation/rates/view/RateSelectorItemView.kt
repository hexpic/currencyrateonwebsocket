package ru.aezhkov.currencyrate.presentation.rates.view

import android.content.Context
import android.util.AttributeSet
import android.widget.CheckBox
import android.widget.FrameLayout
import ru.aezhkov.currencyrate.R
import ru.aezhkov.currencyrate.presentation.base.extension.bindView
import ru.aezhkov.currencyrate.presentation.base.widget.Widget
import ru.aezhkov.currencyrate.presentation.rates.model.RateSelectorItemUiModel

class RateSelectorItemView
@JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle), Widget<RateSelectorItemUiModel> {

    private val checkboxView by bindView<CheckBox>(R.id.rate_selector_item_checkbox)

    override fun bind(model: RateSelectorItemUiModel) {
        checkboxView.text = model.title
        checkboxView.isChecked = model.checked

        checkboxView.setOnCheckedChangeListener { _, isChecked ->
            model.checkedListener?.invoke(model.pair, isChecked)
        }
    }
}