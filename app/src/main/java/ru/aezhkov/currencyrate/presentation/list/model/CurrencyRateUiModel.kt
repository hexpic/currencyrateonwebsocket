package ru.aezhkov.currencyrate.presentation.list.model

import ru.aezhkov.currencyrate.data.model.CurrencyPair

data class CurrencyRateUiModel(
    val pair: CurrencyPair,
    val instrumentName: String,
    val value: String,
    val spread: String
) {
    var longClickListener: ((CurrencyPair) -> Unit)? = null
}
