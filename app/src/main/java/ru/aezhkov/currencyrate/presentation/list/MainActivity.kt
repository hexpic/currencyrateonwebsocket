package ru.aezhkov.currencyrate.presentation.list

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import ru.aezhkov.currencyrate.R
import ru.aezhkov.currencyrate.presentation.base.RateApp
import ru.aezhkov.currencyrate.presentation.base.extension.bindView
import ru.aezhkov.currencyrate.presentation.list.model.CurrencyRateUiModel
import ru.aezhkov.currencyrate.presentation.list.view.CurrencyRateAdapter
import ru.aezhkov.currencyrate.presentation.list.view.CurrencyRateItemView
import ru.aezhkov.currencyrate.presentation.rates.RatesSelectorActivity
import javax.inject.Inject

class MainActivity : MvpAppCompatActivity(), CurrencyRateListView {

    @InjectPresenter
    @Inject
    lateinit var presenter: CurrencyRateListPresenter

    @ProvidePresenter
    fun providePresenter(): CurrencyRateListPresenter = presenter

    private val listView by bindView<RecyclerView>(R.id.currency_rate_list)

    private val nameView by bindView<TextView>(R.id.currency_rate_item_name)
    private val valueView by bindView<TextView>(R.id.currency_rate_item_value)
    private val spreadView by bindView<TextView>(R.id.currency_rate_item_spread)

    private val adapter = CurrencyRateAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as RateApp).currencyRateListComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nameView.text = "Instrument"
        valueView.text = "BID / ASK"
        spreadView.text = "SPREAD"

        listView.adapter = adapter
        listView.layoutManager = LinearLayoutManager(this)
    }

    override fun updateList(models: List<CurrencyRateUiModel>) {
        adapter.submitList(models)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        MenuInflater(this).inflate(R.menu.currency_list_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.list -> startActivity(Intent(this, RatesSelectorActivity::class.java))
            R.id.sort_spread -> presenter.sortSpread()
        }

        return super.onOptionsItemSelected(item)
    }

}
