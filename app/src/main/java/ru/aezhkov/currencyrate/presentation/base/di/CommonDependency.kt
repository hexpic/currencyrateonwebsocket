package ru.aezhkov.currencyrate.presentation.base.di

import android.app.Application
import ru.aezhkov.currencyrate.data.CurrencyPairStorage
import ru.aezhkov.currencyrate.domain.usecase.CurrencyPairVisibilityUseCase

interface CommonDependency {
    fun application(): Application
    fun provideCurrencyPairVisibilityUseCase(): CurrencyPairVisibilityUseCase
    fun bindCurrencyPairStorage(): CurrencyPairStorage
}