package ru.aezhkov.currencyrate.presentation.base.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import ru.aezhkov.currencyrate.data.di.SingletonDataBinder
import ru.aezhkov.currencyrate.domain.di.SingletonDomainBinder
import ru.aezhkov.currencyrate.presentation.rates.di.RatesSelectorComponent
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        CommonModule::class,
        SingletonDomainBinder::class,
        SingletonDataBinder::class
    ]
)
interface ApplicationComponent : CommonDependency{

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }
}