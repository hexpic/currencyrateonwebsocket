package ru.aezhkov.currencyrate.presentation.list.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.aezhkov.currencyrate.presentation.base.widget.Widget
import ru.aezhkov.currencyrate.presentation.list.model.CurrencyRateUiModel
import ru.aezhkov.currencyrate.R


class CurrencyRateAdapter : ListAdapter<CurrencyRateUiModel, CurrencyRateViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyRateViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val rateItemView = inflater.inflate(R.layout.currency_rate_item_view, parent, false)
        return CurrencyRateViewHolder(rateItemView)
    }

    override fun onBindViewHolder(holder: CurrencyRateViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<CurrencyRateUiModel>() {

            override fun areItemsTheSame(oldItem: CurrencyRateUiModel, newItem: CurrencyRateUiModel): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: CurrencyRateUiModel, newItem: CurrencyRateUiModel): Boolean {
                return oldItem == newItem
            }
        }
    }

}

class CurrencyRateViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(model: CurrencyRateUiModel) {
        itemView as Widget<CurrencyRateUiModel>
        itemView.bind(model)
    }

}