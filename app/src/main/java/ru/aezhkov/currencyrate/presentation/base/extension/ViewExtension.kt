package ru.aezhkov.currencyrate.presentation.base.extension

import android.app.Activity
import android.view.View
import androidx.annotation.IdRes

@Suppress("UNCHECKED_CAST")
fun <T> View.bindView(@IdRes res: Int): Lazy<T> = lazy { findViewById<View>(res) as T }

@Suppress("UNCHECKED_CAST")
fun <T> Activity.bindView(@IdRes res: Int): Lazy<T> = lazy { findViewById<View>(res) as T }