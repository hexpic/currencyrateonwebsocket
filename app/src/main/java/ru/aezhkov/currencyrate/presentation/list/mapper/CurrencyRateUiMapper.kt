package ru.aezhkov.currencyrate.presentation.list.mapper

import ru.aezhkov.currencyrate.domain.model.CurrencyModel
import ru.aezhkov.currencyrate.presentation.list.model.CurrencyRateUiModel
import javax.inject.Inject

class CurrencyRateUiMapper
@Inject constructor() {

    fun map(model: CurrencyModel): CurrencyRateUiModel {
        return CurrencyRateUiModel(
            pair = model.pair,
            instrumentName = model.instrument,
            value = "${model.bid}/${model.ask}",
            spread = model.spread
        )
    }
}
