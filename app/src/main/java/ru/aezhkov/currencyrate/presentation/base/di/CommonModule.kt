package ru.aezhkov.currencyrate.presentation.base.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class CommonModule {

    @Provides
    fun provideApplicationContext(application: Application): Context {
        return application
    }
}