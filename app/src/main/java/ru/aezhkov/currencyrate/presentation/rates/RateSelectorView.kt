package ru.aezhkov.currencyrate.presentation.rates

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType
import ru.aezhkov.currencyrate.presentation.rates.model.RateSelectorItemUiModel

@StateStrategyType(AddToEndSingleStrategy::class)
interface RateSelectorView : MvpView {

    fun updateList(models: List<RateSelectorItemUiModel>)

}