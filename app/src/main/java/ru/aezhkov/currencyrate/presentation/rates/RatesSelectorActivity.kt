package ru.aezhkov.currencyrate.presentation.rates

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import ru.aezhkov.currencyrate.R
import ru.aezhkov.currencyrate.presentation.base.RateApp
import ru.aezhkov.currencyrate.presentation.base.extension.bindView
import ru.aezhkov.currencyrate.presentation.rates.model.RateSelectorItemUiModel
import ru.aezhkov.currencyrate.presentation.rates.view.RateSelectorAdapter
import javax.inject.Inject

class RatesSelectorActivity : MvpAppCompatActivity(), RateSelectorView {
    @InjectPresenter
    @Inject
    lateinit var presenter: RatesSelectorPresenter

    @ProvidePresenter
    fun providePresenter(): RatesSelectorPresenter = presenter

    private val rateList by bindView<RecyclerView>(R.id.rate_selector_list)

    private val adapter = RateSelectorAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as RateApp).rateSelectorListComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rate_selector_activity)

        rateList.adapter = adapter
        rateList.layoutManager = LinearLayoutManager(this)
    }

    override fun updateList(models: List<RateSelectorItemUiModel>) {
        adapter.submitList(models)
    }
}