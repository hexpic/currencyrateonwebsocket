package ru.aezhkov.currencyrate.presentation.rates.di

import android.app.Application
import dagger.Component
import ru.aezhkov.currencyrate.data.CurrencyPairStorage
import ru.aezhkov.currencyrate.data.di.DataModule
import ru.aezhkov.currencyrate.domain.di.DomainBinder
import ru.aezhkov.currencyrate.domain.usecase.CurrencyPairVisibilityUseCase
import ru.aezhkov.currencyrate.presentation.base.di.CommonDependency
import ru.aezhkov.currencyrate.presentation.base.di.CommonModule
import ru.aezhkov.currencyrate.presentation.list.di.CurrencyRateListComponent
import ru.aezhkov.currencyrate.presentation.rates.RatesSelectorActivity

@Component(
    modules = [
        DataModule::class,
        DomainBinder::class,
        CommonModule::class
    ],
    dependencies = [CommonDependency::class]
)
interface RatesSelectorComponent {

    fun inject(target: RatesSelectorActivity)

    @Component.Builder
    interface Builder {

        fun dependency(dependency: CommonDependency): RatesSelectorComponent.Builder

        fun build(): RatesSelectorComponent
    }
}