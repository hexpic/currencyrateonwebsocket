package ru.aezhkov.currencyrate.presentation.list.view

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import ru.aezhkov.currencyrate.presentation.base.extension.bindView
import ru.aezhkov.currencyrate.presentation.base.widget.Widget
import ru.aezhkov.currencyrate.presentation.list.model.CurrencyRateUiModel
import ru.aezhkov.currencyrate.R

class CurrencyRateItemView
@JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle), Widget<CurrencyRateUiModel> {

    private val nameView by bindView<TextView>(R.id.currency_rate_item_name)
    private val valueView by bindView<TextView>(R.id.currency_rate_item_value)
    private val spreadView by bindView<TextView>(R.id.currency_rate_item_spread)

    override fun bind(model: CurrencyRateUiModel) {
        nameView.text = model.instrumentName
        valueView.text = model.value
        spreadView.text = model.spread

        setOnLongClickListener {
            model.longClickListener?.invoke(model.pair)
            return@setOnLongClickListener true
        }
    }
}