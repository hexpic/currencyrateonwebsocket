package ru.aezhkov.currencyrate.presentation.list.di

import dagger.Component
import ru.aezhkov.currencyrate.data.di.DataModule
import ru.aezhkov.currencyrate.domain.di.DomainBinder
import ru.aezhkov.currencyrate.presentation.base.di.CommonDependency
import ru.aezhkov.currencyrate.presentation.base.di.CommonModule
import ru.aezhkov.currencyrate.presentation.list.MainActivity

@Component(
    modules = [
        DataModule::class,
        DomainBinder::class,
        CommonModule::class
    ],
    dependencies = [CommonDependency::class]
)
interface CurrencyRateListComponent {

    fun inject(target: MainActivity)

    @Component.Builder
    interface Builder {

        fun dependency(dependency: CommonDependency): Builder

        fun build(): CurrencyRateListComponent
    }
}