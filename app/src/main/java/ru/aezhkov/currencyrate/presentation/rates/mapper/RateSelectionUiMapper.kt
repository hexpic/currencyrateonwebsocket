package ru.aezhkov.currencyrate.presentation.rates.mapper

import ru.aezhkov.currencyrate.domain.model.CurrencyPairVisibilityModel
import ru.aezhkov.currencyrate.presentation.rates.model.RateSelectorItemUiModel
import javax.inject.Inject

class RateSelectionUiMapper
@Inject constructor() {

    fun map(model: CurrencyPairVisibilityModel): RateSelectorItemUiModel {
        return RateSelectorItemUiModel(
            pair = model.pair,
            title = model.pair.name,// possible to change it if needed
            checked = model.isVisibile
        )
    }
}