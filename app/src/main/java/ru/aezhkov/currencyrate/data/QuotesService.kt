package ru.aezhkov.currencyrate.data

import com.tinder.scarlet.State
import com.tinder.scarlet.WebSocket
import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import io.reactivex.Flowable
import ru.aezhkov.currencyrate.data.model.CurrencyPairValueResponse

interface QuotesService {
    @Receive
    fun observeWebSocketEvent(): Flowable<WebSocket.Event>

    @Send
    fun sendSubscribe(request: String)

    @Send
    fun sendUnsubscribe(request: String)

    @Receive
    fun observeTicker(): Flowable<CurrencyPairValueResponse>

    @Receive
    fun observeState(): Flowable<State>
}

