package ru.aezhkov.currencyrate.data.model

enum class CurrencyPair {
    EUR_USD,
    BTC_USD
}