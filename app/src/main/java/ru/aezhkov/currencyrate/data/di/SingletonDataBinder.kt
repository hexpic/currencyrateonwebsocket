package ru.aezhkov.currencyrate.data.di

import dagger.Binds
import dagger.Module
import ru.aezhkov.currencyrate.data.CurrencyPairStorage
import ru.aezhkov.currencyrate.data.CurrencyPairStorageImpl
import ru.aezhkov.currencyrate.data.repository.CurrencyPairRateRepositoryImpl
import ru.aezhkov.currencyrate.domain.repository.CurrencyPairRateRepository
import javax.inject.Singleton


@Module
interface SingletonDataBinder {

    @Singleton
    @Binds
    fun bindCurrencyPairStorage(impl: CurrencyPairStorageImpl): CurrencyPairStorage
}