package ru.aezhkov.currencyrate.data.model

import com.google.gson.annotations.SerializedName

data class UnsubscribeRequest(
    @SerializedName("UNSUBSCRIBE")
    val productId: String
)