package ru.aezhkov.currencyrate.data.di

import android.app.Application
import com.tinder.scarlet.Lifecycle
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.lifecycle.android.AndroidLifecycle
import com.tinder.scarlet.messageadapter.gson.GsonMessageAdapter
import com.tinder.scarlet.streamadapter.rxjava2.RxJava2StreamAdapterFactory
import com.tinder.scarlet.websocket.okhttp.newWebSocketFactory
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import ru.aezhkov.currencyrate.data.adapter.RateMessageAdapter
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


private const val SOCKET_BASE_URL = "wss://quotes.eccalls.mobi:18400"
//private const val SOCKET_BASE_URL = "wss://ws-feed.gdax.com"

@Module(
    includes = [
        DataBinder::class,
        ServiceModule::class
    ]
)
class DataModule {

    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val trustAllCerts = arrayOf(
            object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<X509Certificate?>?, authType: String?) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<X509Certificate?>?, authType: String?) {
                }
                override fun getAcceptedIssuers():Array<X509Certificate?>?{
                    return arrayOf()
                }
            }
        )
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, SecureRandom())

        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

            .sslSocketFactory(sslContext.socketFactory, trustAllCerts[0] as X509TrustManager)
            .hostnameVerifier { _, _ -> true }
            .build()

    }

    @Provides
    fun provideScarlet(client: OkHttpClient, lifecycle: Lifecycle): Scarlet {

        return Scarlet.Builder()
            .webSocketFactory(client.newWebSocketFactory(SOCKET_BASE_URL))
            .addMessageAdapterFactory(RateMessageAdapter.Factory())
            .addStreamAdapterFactory(RxJava2StreamAdapterFactory())
            .lifecycle(lifecycle)
            .build()
    }

    @Provides
    fun provideLifecycle(application: Application): Lifecycle {
        return AndroidLifecycle.ofApplicationForeground(application)
    }
}