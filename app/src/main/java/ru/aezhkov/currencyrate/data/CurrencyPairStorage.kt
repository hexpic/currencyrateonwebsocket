package ru.aezhkov.currencyrate.data

import ru.aezhkov.currencyrate.data.model.CurrencyPair

interface CurrencyPairStorage {
    fun setValue(pair: CurrencyPair, visibility: Boolean)

    fun getValue(pair: CurrencyPair): Boolean
}