package ru.aezhkov.currencyrate.data.di

import dagger.Binds
import dagger.Module
import ru.aezhkov.currencyrate.data.CurrencyPairStorage
import ru.aezhkov.currencyrate.data.CurrencyPairStorageImpl
import ru.aezhkov.currencyrate.data.repository.CurrencyPairRateRepositoryImpl
import ru.aezhkov.currencyrate.domain.repository.CurrencyPairRateRepository

@Module
interface DataBinder {
    @Binds
    fun bindCurrencyPairRateRepository(impl: CurrencyPairRateRepositoryImpl): CurrencyPairRateRepository
}