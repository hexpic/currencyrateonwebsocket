package ru.aezhkov.currencyrate.data

import android.content.Context
import ru.aezhkov.currencyrate.data.model.CurrencyPair
import javax.inject.Inject

private const val PREFERENCE_NAME = "rates_visibility"

class CurrencyPairStorageImpl
@Inject constructor(
    context: Context
) : CurrencyPairStorage {

    private val sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    @Synchronized
    override fun setValue(pair: CurrencyPair, visibility: Boolean) {
        sharedPreferences.edit().putBoolean(pair.name, visibility).apply()
    }

    @Synchronized
    override fun getValue(pair: CurrencyPair): Boolean {
        return sharedPreferences.getBoolean(pair.name, true)
    }
}