package ru.aezhkov.currencyrate.data.model

import com.google.gson.annotations.SerializedName

data class CurrencyPairValueResponse(
    @SerializedName("subscribed_count")
    val subscribed_count: Int?,
    @SerializedName("subscribed_list")
    val subscribedList: SubscribedList?,
    @SerializedName("ticks")
    val ticks: List<TickResponse>?
) {
    data class SubscribedList(
        @SerializedName("ticks")
        val ticks: List<TickResponse>
    )

    data class TickResponse(
        @SerializedName("s")
        val instrument: String,
        @SerializedName("b")
        val bid: String,
        @SerializedName("a")
        val ask: String,
        @SerializedName("spr")
        val spread: String
    )
}