package ru.aezhkov.currencyrate.data.repository

import com.tinder.scarlet.State
import io.reactivex.Flowable
import okhttp3.WebSocketListener
import ru.aezhkov.currencyrate.data.QuotesService
import ru.aezhkov.currencyrate.data.model.CurrencyPair
import ru.aezhkov.currencyrate.domain.model.CurrencyModel
import ru.aezhkov.currencyrate.domain.repository.CurrencyPairRateRepository
import javax.inject.Inject

class CurrencyPairRateRepositoryImpl
@Inject constructor(
    private val service: QuotesService
) : WebSocketListener(), CurrencyPairRateRepository {

    override fun subscribe(pair: CurrencyPair) {
        val request = pairValueMap[pair] ?: throw IllegalArgumentException("Unknown pair $pair")
        service.sendSubscribe("SUBSCRIBE: $request")
    }

    override fun unsubscribe(pair: CurrencyPair) {
        val request = pairValueMap[pair] ?: throw IllegalArgumentException("Unknown pair $pair")
        service.sendUnsubscribe("UNSUBSCRIBE: $request")
    }

    override fun observe(): Flowable<CurrencyModel> {
        return service.observeTicker()
            .map { response ->
                val tick = response.ticks?.first() ?: response.subscribedList?.ticks?.first()
                with(tick!!) {
                    val pair = instrumentMap[instrument] ?: throw IllegalStateException("Unknown instrument $instrument")
                    CurrencyModel(pair, instrument, bid, ask, spread)
                }
            }
    }

    override fun observeState(): Flowable<State> {
        return service.observeState()
    }

    companion object {
        private val pairValueMap = mapOf(
            CurrencyPair.EUR_USD to "EURUSD",
            CurrencyPair.BTC_USD to "BTCUSD"
        )
        private val instrumentMap = mapOf(
            "EURUSD" to CurrencyPair.EUR_USD,
            "BTCUSD" to CurrencyPair.BTC_USD
        )
    }

}