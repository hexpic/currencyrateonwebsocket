package ru.aezhkov.currencyrate.data.model

import com.google.gson.annotations.SerializedName

data class SubscribeRequest(
    @SerializedName("SUBSCRIBE")
    val productId: String
)