package ru.aezhkov.currencyrate.data.di

import com.tinder.scarlet.Scarlet
import dagger.Module
import dagger.Provides
import ru.aezhkov.currencyrate.data.QuotesService

@Module
class ServiceModule {

    @Provides
    fun provideQuotesService(scarlet: Scarlet): QuotesService {
        return scarlet.create(QuotesService::class.java)
    }
}